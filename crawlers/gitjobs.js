const axios = require('axios')
module.exports = function() {
  return new Promise((resolve, reject) => {
    axios.get('https://jobs.github.com/positions.json?location=remote')
    .then(response => {
      let jobs = []
      const json = response.data
      json.forEach(obj => {
        jobs.push({
          jobId: obj.id,
          category: 'programming',
          site: 'jobs.github.com',
          company: obj.company,
          link: obj.url,
          tags: [],
          title: obj.title,
          description: obj.description,
          publishedDate: new Date(obj.created_at),
          imageUrl: obj.company_logo,
        })
      })
  
      resolve(jobs)
    })
    .catch(err => reject(err))
  })
}
