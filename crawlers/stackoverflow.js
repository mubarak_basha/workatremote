const axios = require('axios')
const parseString = require('xml2js').parseString
module.exports = () => {
  return new Promise((resolve, reject) => {
    axios.get('https://stackoverflow.com/jobs/feed?r=true')
      .then(response => {
        parseString(response.data, (err, result) => {
          if(err){
            reject(err)
          }
          const jobs = []
          result.rss.channel[0].item.forEach(item => {
            const title = item.title[0].replace('(allows remote)', '').replace('()', '').trim()
            // const titleRaw = item.title[0];
            // const reForParan = /\(.*\)/ig;
            // const atOmittedValue = titleRaw.substr(0, titleRaw.indexOf(' at '))
            // let title = atOmittedValue.replace(reForParan, '')
            //                     .replace(/,.*/g, '')
            //                     .replace(/ \/.*/g, '')
            //                     .replace(/remote/ig, '')
            //                     .replace(/anywhere/ig, '')
            //                     .replace(/\[.*\]$/g, '')
            //                     .replace(/ @ .*/g, '');
            // const indexOfEngineer = title.indexOf('Engineer');
            // if(indexOfEngineer > -1)
            // {
            //     title = title.substr(0, indexOfEngineer+8)
            // }
            jobs.push({
              jobId: item.guid[0]._,
              category: 'programming',
              site: 'stackoverflow.com',
              company: item['a10:author'][0]['a10:name'][0],
              link: item.link[0],
              tags: item.category,
              title: title,
              description: item.description[0],
              publishedDate: new Date(item.pubDate[0]),
              imageUrl: null
            })
          })
          resolve(jobs)
        })
      })
      .catch(err => {
        console.log(`stackoverflow: error while retriving \n ${err}`)
      })
  })
}