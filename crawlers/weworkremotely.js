const axios = require('axios')
const parseString = require('xml2js').parseString
const html2text = require('html-to-text')
const links = {
  'programming': 'https://weworkremotely.com/categories/remote-programming-jobs.rss',
  'customer-support': 'https://weworkremotely.com/categories/remote-customer-support-jobs.rss',
  'design': 'https://weworkremotely.com/categories/remote-design-jobs.rss',
  'devops': 'https://weworkremotely.com/categories/remote-devops-sysadmin-jobs.rss',
  'copywriting': 'https://weworkremotely.com/categories/remote-copywriting-jobs.rss',
  'business-management' : 'https://weworkremotely.com/categories/business-and-management.rss',
  'other': 'https://weworkremotely.com/categories/remote-jobs.rss',
  'sales-marketing': 'https://weworkremotely.com/categories/sales-and-marketing.rss',
  'product': 'https://weworkremotely.com/categories/product.rss'
}

module.exports = (category) => {
  return new Promise((resolve, reject) => {
    axios.get(links[category])
      .then(response => {
        parseString(response.data, (err, result) => {
          if(err) {
            reject(err)
          }
          const jobs = []
          result.rss.channel[0].item.forEach(item => {
            const imageUrl = item["media:content"] != undefined ? item["media:content"][0]['$'].url : null
            const title = item.title[0]
            const company = title.substr(0, title.indexOf(':')).trim()
            const description = html2text.fromString(item.description[0].replace(/<img[^>]*>/g, ""), {wordwrap: false})
            const publishedDateString = item.pubDate[0]
            const id = item.guid[0]
            const link = item.link[0]
            jobs.push({
              jobId: id,
              category: category,
              site: 'weworkremotely.com',
              company: company,
              link: link,
              tags: [],
              title: title,
              description: description,
              publishedDate: new Date(publishedDateString),
              imageUrl: imageUrl
            })
          })
          resolve(jobs)
        })
      })
      .catch(err => {
        console.log(`weworkremotely: ${category} error while retriving \n ${err}`)
      })
  })
}