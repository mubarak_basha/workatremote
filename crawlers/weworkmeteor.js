const axios = require('axios')

module.exports = () => {
  return new Promise((resolve, reject) => {
    axios.get('https://www.weworkmeteor.com/api/jobs')
      .then(response => {
        let jobs = []
        const json = response.data.data
        json.forEach(obj => {
          if(obj.remote){
            jobs.push({
              jobId: obj._id,
              category: 'programming',
              site: 'weworkmeteor.com',
              company: obj.company,
              headquaters: obj.location,
              link: obj.siteUrl,
              tags: ['meteor'],
              title: obj.title,
              description: obj.description,
              publishedDate: new Date(obj.updatedAt)
            })
          }
        })

        resolve(jobs)
      })
      .catch(err => reject(err))
  })
}