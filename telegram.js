const TelegramBot = require('node-telegram-bot-api')


module.exports = (siteURL) => {
  const telegramToken = process.env.TELEGRAM_TOKEN
  const bot = new TelegramBot(telegramToken)
  bot.setWebHook(`${siteURL}/bot${telegramToken}`)
  const sendMessageToMubarak = (message) => {
    bot.sendMessage(process.env.TELEGRAM_MUBARAK_GROUP, message, {parse_mode: 'HTML', disable_web_page_preview: true})
  }
  const sendMessageToGroup = (message) => {
    bot.sendMessage(process.env.TELEGRAM_PUBLIC_GROUP, message, {parse_mode: 'HTML'})
  }
  return {
    telegramToken,
    sendMessageToMubarak,
    sendMessageToGroup
  }
}