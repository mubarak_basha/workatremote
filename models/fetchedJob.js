const mongoose = require('mongoose')

const fetchedJob = new mongoose.Schema({
  category: String,
  site: String,
  jobId: String,
  title: String,
  company: String,
  description: String,
  publishedDate: Date,
  link: {type: String, unique: true},
  tags: [String],
  imageUrl: String,
  createdOn: {type: Date, default: Date.now}
})

module.exports = mongoose.model('FetchedJob', fetchedJob)