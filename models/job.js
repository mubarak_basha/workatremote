const mongoose = require('mongoose')
mongoose.Promise = global.Promise
const slug = require('slugs')

const jobSechema = new mongoose.Schema({
  category: String,
  site: String,
  jobId: String,
  title: String,
  company: String,
  twitter: String,
  description: String,
  publishedDate: Date,
  link: {type: String, unique: true},
  tags:[String],
  imageUrl: String,
  createdOn: {type: Date, default: Date.now},

  salary: String,
  timezone: String,
  experience: String,
  degree: String,
  country: String,
  slug: {type: String, unique: true},

  newJob: {type: Boolean, default: false},

  //only for programming
  languages: [String],
  frameworks: [String],
  platforms: [String],
  tools: [String],
  databases: [String]
})

jobSechema.pre('save', async function(next){
  if(!this.isModified('title')){
    next()
    return
  }

  this.slug = slug(this.title)
  const slugRegEx = new RegExp(`^(${this.slug})((-[0-9]*$)?)`, 'i')
  const jobsWithSlug = await this.constructor.find({slug: slugRegEx})
  if(jobsWithSlug.length){
    this.slug = `${this.slug}-${jobsWithSlug.length + 1}`
  }
  next()
})

module.exports = mongoose.model('Job', jobSechema)