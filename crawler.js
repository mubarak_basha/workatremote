//node modules
const fs = require('fs')
//installed modules
const moment = require('moment')
const mongoose = require('mongoose')
const axios = require('axios')
const cheerio = require('cheerio')
//custom modules
const google = require('./google')
const twitter = require('./twitter')
const telegram = require('./telegram')('https://workatremote.com')
let gitJobs = require('./crawlers/gitjobs')
let stackoverflowJobs = require('./crawlers/stackoverflow')
let weworkmeteor = require('./crawlers/weworkmeteor')
let weworkremotely = require('./crawlers/weworkremotely')
//models
const Job = mongoose.model('Job')

module.exports = (siteURL) => {
  const getJSONFromFile = (filepath) => {
    return new Promise((resolve, reject) => {
      fs.readFile(filepath, (err, data) => {
        if(err) {
          reject(err)
        }
        let json = JSON.parse(data)
        resolve(json)
      })
    })
  }

  const getMatchedTags = (content, filepath) => {
    return new Promise((resolve, reject) => {
      let tags = []
      fs.readFile(filepath, (err, data) => {
        if (err) {
          reject(err)
        }
        let json = JSON.parse(data)
        for (regExMatch in json) {
          if (RegExp(regExMatch, 'i').test(content)) {
            tags.push(json[regExMatch])
          }
        }
        resolve(tags)
      })
    })
  }

  const generateTagsFromContent = async (content) => {
    let frameworks = await getMatchedTags(content, './tags/frameworks.json')
    let languages = await getMatchedTags(content, './tags/languages.json')
    let databases = await getMatchedTags(content, './tags/databases.json')
    let platforms = await getMatchedTags(content, './tags/platforms.json')
    let tools = await getMatchedTags(content, './tags/tools.json')
    let tweetTags = await getJSONFromFile('./tags/twitter.json')
    //let tags = frameworks.concat(languages).concat(databases).concat(platforms).concat(tools)
    let tags = [...frameworks, ...languages, ...databases, ...platforms, ...tools]
    let twitterTags = tags.map(tag => tweetTags[tag]).filter(tag => tag != null)
    return {frameworks, languages, databases, platforms, tools, tags, twitterTags}
  }

  const getCompanyTwitterUsernameFromDB = async (company) => {
    let twitterFound = await Job.findOne({company: company}, ["twitter"]).where('twitter').ne('')
    if(twitterFound != null) {
      return twitterFound.twitter
    }
    return ''
  }

  const getSalaryAndTwitterFromStackoverflowJobSite = async (link, companyTwitter) => {
    let stackJobPage = await axios.get(link)
    const $Job = cheerio.load(stackJobPage.data)
    const salary = $Job('.job-details--header span.-salary').text().replace('| equity', '').replace('| Equity', '').trim()
    let twitter = companyTwitter
    if(twitter == ''){
      let stackURI = $Job('.job-details--header a.fc-black-700').attr('href')
      if(stackURI != undefined && stackURI.startsWith('/')) {
        let stackCompanyPage = await axios.get(`https://stackoverflow.com${stackURI}`)
        const $Company = cheerio.load(stackCompanyPage.data)
        $Company('#right-column span.ps-absolute a').each(function(index, element) {
          let href = decodeURIComponent($Company(this).attr('href'))
          if(href.includes('twitter')){
            twitter = '@'+href.replace('/jobs/companies/a/ext-link?redirectUrl=http://twitter.com/', '').replace('/jobs/companies/a/ext-link?redirectUrl=https://twitter.com/', '').replace('&externalLink=Twitter', '')
          }
        })
      }
    }
    return {salary, twitter}
  }

  const finalizeJob = async (job) => {
    let content = job.title + ' ' + job.description
    let {frameworks, languages, databases, platforms, tools, tags, twitterTags} = await generateTagsFromContent(content)
    let companyTwitter = await getCompanyTwitterUsernameFromDB(job.company)
    let salary = ''
    
    if(job.site == 'stackoverflow.com') {
      let salaryAndTwitter = await getSalaryAndTwitterFromStackoverflowJobSite(job.link, companyTwitter)
      salary = salaryAndTwitter.salary
      companyTwitter = salaryAndTwitter.twitter
    }
    job['frameworks'] = frameworks
    job['languages'] = languages
    job['databases'] = databases
    job['platforms'] = platforms
    job['tools'] = tools
    job['tags'] = tags.concat(job.tags)
    job['salary'] = salary
    job['twitter'] = companyTwitter
    //TODO - In the future add automatic timezone detection.
    job['timezone'] = ''
    return {job, twitterTags}
  }

  const isDateInLastSevenDays = (dateString) => {
    const seventhDay = moment().subtract(7, "d").toDate()
    seventhDay.setHours(0, 0, 0, 0)
    return moment(dateString).isSameOrAfter(seventhDay)
  }

  const filterJobsOfthisWeek = (jobs) => jobs.filter(job => isDateInLastSevenDays(job.publishedDate))

  const addCategoriesToJobs = (jobs, tags) => {
    return jobs.map(job => {
      job['tags'] = []
      if(job.title.toLowerCase().includes('front-end') || job.title.toLowerCase().includes('frontend')){
        job.tags.push('front-end')
      }
      job.tags = job.tags.concat(tags)
      return job
    })
  }

  
  const saveJob =  job => {
    let mJob = new Job({
      category: job.category,
      site: job.site,
      jobId: job.jobId,
      title: job.title,
      company: job.company,
      twitter: job.twitter,
      description: job.description,
      publishedDate: job.publishedDate,
      link: job.link,
      tags: job.tags,
      imageUrl: job.imageUrl,
      salary: job.salary,
      timezone: job.timezone,
      experience: '',
      degree: '',
      country: '',
      languages: job.languages,
      frameworks: job.frameworks,
      platforms: job.platforms,
      tools: job.tools,
      databases: job.databases
    })
    
    return mJob.save()
  }

  const fetchJobs = async () => {
    let allJobs = {}
    gitJobs()
      .then(jobs => {
        allJobs['git'] = addCategoriesToJobs(filterJobsOfthisWeek(jobs), ['programming'])
        return stackoverflowJobs()
      })
      .then(jobs => {
        allJobs['stackoverflow'] = addCategoriesToJobs(filterJobsOfthisWeek(jobs), ['programming'])
        return weworkmeteor()
      })
      .then(jobs => {
        allJobs['weworkmeteor'] = addCategoriesToJobs(filterJobsOfthisWeek(jobs), ['programming'])
        return weworkremotely('programming')
      })
      .then(jobs => {
        allJobs['weworkremotely-programming'] = addCategoriesToJobs(filterJobsOfthisWeek(jobs), ['programming'])
        return weworkremotely('customer-support')
      })
      .then(jobs => {
        allJobs['weworkremotely-customer-support'] = addCategoriesToJobs(filterJobsOfthisWeek(jobs), ['customer-support'])
        return weworkremotely('design')
      })
      .then(jobs => {
        allJobs['weworkremotely-design'] = addCategoriesToJobs(filterJobsOfthisWeek(jobs), ['design'])
        return weworkremotely('devops')
      })
      .then(jobs => {
        allJobs['weworkremotely-devops'] = addCategoriesToJobs(filterJobsOfthisWeek(jobs), ['programming'])
        return weworkremotely('copywriting')
      })
      .then(jobs => {
        allJobs['weworkremotely-copywriting'] = addCategoriesToJobs(filterJobsOfthisWeek(jobs), ['non-tech', 'other'])
        return weworkremotely('business-management')
      })
      .then(jobs => {
        allJobs['weworkremotely-business-management'] = addCategoriesToJobs(filterJobsOfthisWeek(jobs), ['non-tech', 'other'])
        return weworkremotely('sales-marketing')
      })
      .then(jobs => {
        allJobs['weworkremotely-sales-marketing'] = addCategoriesToJobs(filterJobsOfthisWeek(jobs), ['marketing', 'sales'])
        return weworkremotely('product')
      })
      .then(jobs => {
        allJobs['weworkremotely-product'] = addCategoriesToJobs(filterJobsOfthisWeek(jobs), ['non-tech', 'other'])
        return weworkremotely('other')
      })
      .then(jobs => {
        allJobs['weworkremotely-other'] = addCategoriesToJobs(filterJobsOfthisWeek(jobs), ['non-tech', 'other'])
        return Promise.resolve(allJobs)
      })
      .then(async jobs => {
        let concatJobs = []
        for(let key in jobs){
          if(jobs.hasOwnProperty(key)){
            concatJobs = concatJobs.concat(allJobs[key])
          }
        }
        let dbLinks = await Job.find({}, ["link"])
        let justLinks = dbLinks.map(dbLink => dbLink.link)
        concatJobs.forEach(async fJob => {
          if(justLinks.indexOf(fJob.link) == -1)
          {
            let {job, twitterTags} = await finalizeJob(fJob)
            saveJob(job)
            .then(response => {
              //notify google about new job.
              google.submitURLForIndexing(`${siteURL}/${response.slug}`)
              //send message to mubarak that a new job has been posted.
              telegram.sendMessageToMubarak(`Job Published category: ${response.category} title: ${response.title} <a href="/editJob?id=${response._id}">Edit</a>`)
              //tweet the latest job
              twitter.postTweet(`${response.twitter != '' ? response.twitter : response.company} is looking for ${response.title} learn more at ${siteURL}/${response.slug} #remotework #remotejob #job ${twitterTags.join(',')}`)
              //post a message in work@remote telgram group
              telegram.sendMessageToGroup(`<a href="${siteURL}/${response.slug}">${response.title}</a>\nCompany: ${response.company}\n${response.salary != ''? 'Salary: '+response.salary : ''}\n${response.experience != ''? 'Exp: '+response.experience : ''}\n${response.country != ''? '<b>'+response.country+'</b>' : ''}\n${response.timezone != ''? 'Timezone: '+response.timezone : ''}\nTags: ${twitterTags.join(' ')}`)
            })
            .catch(error => {
              console.log('error', error)
            })
          }
        })
      })
      .catch(err => {
        console.error(err)
      })
  }
  return {
    fetchJobs
  }
}